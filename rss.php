<?php 

	/**
	 * myRss()
	 *  
	 * Jednoducha funkce na zobrazeni informaci z daneho RSS kanalu
	 * 
	 * @author Roman Janko <admin@rjwebdesign.net>
	 * @date 08-05-2008
	 * @version 1.0   
	  
	 * @param string $url  adresa na RSS kanal
	 * @param integer $limit  pocet zobrazozenych polozek
	 * @param mixed $charsetFrom v jakem kodovani rss kanal prebirame, v pripade false se nic nekonvertuje
	 * @param mixed $charsetTo do jakeho kodovani rss zobrazime , v pripade false se nic nekonvertuje
	 * @return string  pozadovany rss vystup nebo informace o chybe
	 */
	function myRss($url, $limit = 10, $charsetFrom = false, $charsetTo = false){
		if ($xml = simplexml_load_file($url)){
	        $counter = 0;
	        $out = array();
	        foreach ($xml->channel->item as $item){
	            if ($charsetFrom != false && $charsetTo != false){
	                $item->title       = iconv($charsetFrom, $charsetTo, $item->title);
	                $item->description = iconv($charsetFrom, $charsetTo, $item->description);   
	            }
	            	            
	            if ($counter == $limit){
	            	break;
	            }
	            
	            $out[$counter] = array($item->title, $item->link, $item->description);
	            $counter++;
	        }	        
	    }
	    else{
	        $out = 'Požadovanou url nelze načíst (<em>' . $url . '</em>).'; 
	    }
	    
	    return $out;
	}

?>