<?php

	function sendMail($to, $subject, $message, $from, $reply, $html = false, $attach = null ){ 
	                            
		/*
		 *   $Attach = array(
		 *     	array( 'name' => 'file name', 'content' => 'file content', 'mime_type' => 'mime/type' )
		 * 	    ...
		 *   );
		 */
	
		$boundary = strtoupper( md5( uniqid( 'bound_' ) ) ); 
	  
		$header = "From: =?UTF-8?B?" . base64_encode( $from ) . "?= <" . $reply . ">\n"; 
		$header .= "MIME-version: 1.0\n"; 
		$header .= "Return-Path: <$reply>\n"; 
		$header .= "Reply-To: $reply\n"; 
		$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\n\n";
		$header .= "--" . $boundary . "\n"; 
		$header .= "Content-Type: text/" . ( ( $html ) ? ( 'html' ) : ( 'plain' ) ) . "; charset=\"UTF-8\"\n";
		$header .= "Content-Transfer-Encoding: base64\n\n"; 
		$header .=  base64_encode( $message ) . "\n\n";
	            
		if ( is_array( $attach ) ){
			reset( $attach );
	
			for ( $i = 0; $i < count( $attach ); ++ $i ){	
				if ( $attach[ key( $attach ) ][ 'content' ] && $attach[ key( $attach ) ][ 'name' ] ){
					$header .= "--" . $boundary . "\n"; 
					$header .= "Content-Type: " . ( ( $attach[ key( $attach ) ][ 'mime_type' ] ) ? ( $attach[ key( $attach ) ][ 'mime_type' ] ) : ( 'application/octet-stream' ) ) . ";\n";
					$header .= "Content-Transfer-Encoding: base64\n"; 
					$header .= "Content-Disposition: attachment; filename=\"" . $attach[ key( $attach ) ][ 'name' ] . "\"\n\n";
					$header .=  chunk_split( base64_encode( $attach[ key( $attach ) ][ 'content' ] ) )  . "\n\n";
				}
				else{
					return false;
				}
	      
				next( $attach );
			}
		}
	             
		$header .= "--" . $boundary . "--"; 
	
		return mail( $to, '=?UTF-8?B?' . base64_encode( $subject ) . '?=', "", $header ); 
	
	}

?>