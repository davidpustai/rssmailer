<?php 

	require_once 'rss.php';
	require_once 'mail.php';
	require_once 'connection.php';
	
	$message = "RSS výpis ze dne " . date("j. n. Y") . ":<br /><br />";
	
	$qry = mysql_query("SELECT title, url FROM sources");
	if(mysql_num_rows($qry) > 0){		
		while($data = mysql_fetch_assoc($qry)){
			$pre_message = "<b><u>" . $data['title'] . " (" . $data['url'] . ")</u></b><br />";
			$output = myRss($data['url']);
			
			if(is_array($output)){
				foreach($output as $item){
					$title = $item[0];
					$link = $item[1];
					$description = $item[2];
					
					$qry2 = mysql_query("SELECT item_id FROM items WHERE title='$title' AND link='$link' AND description='$description'");
					$count = mysql_num_rows($qry2);
					if($count == 0){
						$pre_message .= "<a href=\"" . $link . "\" style=\"color: green;\">" . $title . "</a><br />";
						$pre_message .= $description . "<br /><br />";
						$qry2 = mysql_query("INSERT INTO items (title, link, description, date) VALUES ('" . mysql_real_escape_string($title) . "', '" . mysql_real_escape_string($link) . "', '" . mysql_real_escape_string($description) . "', '" . date("Y-m-d") . "')");
						if(!$qry2){
							$pre_message .= "Chyba při vkládání do DB.<br /><br />";
						}															
					}				
				}
			}
			else{
				$pre_message .= "Chyba načtení RSS zdroje.<br /><br />";
			}
			
			/* zadne novinky */
			if($pre_message != "<b><u>" . $data['title'] . " (" . $data['url'] . ")</u></b><br />"){
				$message .= $pre_message;
			}
		}
		
		/* zadne novinky nikde */
		if($message == "RSS výpis ze dne " . date("j. n. Y") . ":<br /><br />"){
			$message .= "Žádné novinky.<br /><br />";
		}
	}
	else{
		$message .= "Žádné odběry.<br /><br />";
	}
		
	sendMail("david@pustai.cz", "Výpis RSS - " . date("j. n. Y"), $message, "Výpis RSS", "noreply@pustai.cz", true);
	
	mysql_close();

?>